resource "gitlab_group" "this" {
  name        = var.name
  path        = var.path
  description = var.description

  default_branch_protection         = var.branch_protection
  parent_id                         = var.parent_group_id
  prevent_forking_outside_group     = var.allow_external_forks ? false : true
  project_creation_level            = var.developer_can_create_projects ? "developer" : "maintainer"
  request_access_enabled            = var.access_requests
  require_two_factor_authentication = var.require_mfa
  two_factor_grace_period           = var.mfa_grace
  visibility_level                  = var.visibility
}
