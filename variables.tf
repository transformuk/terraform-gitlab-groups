variable "name" {
  description = "Group name."
  type        = string
}

variable "path" {
  description = "Path (namespace) of the group."
  type        = string
  default     = ""
}

variable "branch_protection" {
  description = "Configure default branch protection for projects under this group. Defaults to 1 (partial protection)."
  type        = number
  default     = 1
}

variable "parent_group_id" {
  description = "The ID of the parent group that this resource will be created under."
  type        = string
}

variable "allow_external_forks" {
  description = "Allow projects within this group to be forked external to the group. Defaults to false."
  type        = bool
  default     = false
}

variable "developer_can_create_projects" {
  description = "Allow users with the developer role to create projects in this group. Defaults to false."
  type        = bool
  default     = false
}

variable "require_mfa" {
  description = "Require 2FA for users in this group. Defaults to true."
  type        = bool
  default     = true
}

variable "mfa_grace" {
  description = "Time, in hours, before 2FA is enforced. Defaults to 24 hours."
  type        = number
  default     = 24
}

variable "visibility" {
  description = "Visibility of the group. One of 'private' or 'public'. Defaults to 'private'."
  type        = string
  default     = "private"
}
