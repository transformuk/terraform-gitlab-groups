# terraform-gitlab-groups

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.2.6 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | 3.16.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | 3.16.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_group.this](https://registry.terraform.io/providers/gitlabhq/gitlab/3.16.1/docs/resources/group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_external_forks"></a> [allow\_external\_forks](#input\_allow\_external\_forks) | Allow projects within this group to be forked external to the group. Defaults to false. | `bool` | `false` | no |
| <a name="input_branch_protection"></a> [branch\_protection](#input\_branch\_protection) | Configure default branch protection for projects under this group. Defaults to 1 (partial protection). | `number` | `1` | no |
| <a name="input_developer_can_create_projects"></a> [developer\_can\_create\_projects](#input\_developer\_can\_create\_projects) | Allow users with the developer role to create projects in this group. Defaults to false. | `bool` | `false` | no |
| <a name="input_mfa_grace"></a> [mfa\_grace](#input\_mfa\_grace) | Time, in hours, before 2FA is enforced. Defaults to 24 hours. | `number` | `24` | no |
| <a name="input_name"></a> [name](#input\_name) | Group name. | `string` | n/a | yes |
| <a name="input_parent_group_id"></a> [parent\_group\_id](#input\_parent\_group\_id) | The ID of the parent group that this resource will be created under. | `string` | n/a | yes |
| <a name="input_path"></a> [path](#input\_path) | Path (namespace) of the group. | `string` | `""` | no |
| <a name="input_require_mfa"></a> [require\_mfa](#input\_require\_mfa) | Require 2FA for users in this group. Defaults to true. | `bool` | `true` | no |
| <a name="input_visibility"></a> [visibility](#input\_visibility) | Visibility of the group. One of 'private' or 'public'. Defaults to 'private'. | `string` | `"private"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_id"></a> [id](#output\_id) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
