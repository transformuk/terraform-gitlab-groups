terraform {
  required_version = ">= 1.2.6"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.16.1"
    }
  }
}
